﻿function gridView(id, settings = {}) {
    var grid = document.querySelector("#" + id);
    var template = "\
                    <div class='form-horizontal'> \
        <div class='form-group'> \
            <div class='col-md-1'>Criteria: </div> \
            <div class='col-md-3'> \
                <select class='form-control' name='criteria' required> \
                    <option value=''>--Select Criteria--</option> \
                </select> \
            </div> \
            <div class='col-md-2'> \
                <input type='text' name='value' class='form-control' placeholder='Value' required/> \
            </div> \
            <div class='col-md-2'> \
                <button btn-search type='button' class='btn btn-primary'><i class='glyphicon glyphicon-search'></i> Search</button> \
            </div> \
        </div> \
        <div crt-list class='form-group crt-list'></div> \
    </div> \
    <div style='width: 100%; overflow-x: scroll'> \
                                    <table class='table table-striped' > \
                                        <thead></thead> \
                                        <tbody></tbody> \
                                    </table > \
                                </div > \
                                <div style='margin: 10px 5px; position: relative;'> \
                                <div style='width: 25%; min-width: 100px; display: inline-block'>\
                                    Total Row(s): <span row-count>0</span>\
                                </div>\
                                <div class='text-right' style='width: 70%; min-width: 200px;\
                            display: inline-block; position: absolute; right: 0'>\
                                    Page Size: \
                                    <select id='page-size' class='form-control' \
                                            style='width: 60px; display: inline-block; padding: 8px 6px;'>\
                                        <option value='5'>5</option>\
                                        <option value='10'>10</option>\
                                        <option value='50'>50</option>\
                                        <option value='100'>100</option> \
                                    </select>\
                                    Page: <span id='page'>1</span> \
                                    <button id='btn-prev' type='button' class='btn btn-primary btn-sm'>\
                                        <i class='glyphicon glyphicon-chevron-left'></i> Prev\
                                    </button>\
                                    <button id='btn-next' type='button' class='btn btn-primary btn-sm'>\
                                        Next <i class='glyphicon glyphicon-chevron-right'></i>\
                                    </button>\
                                </div>\
                            </div>";
    grid.innerHTML = template;

    var option = {
        page: 1,
        pageSize: 5,
        criteria: [],
        order: settings.fieldKey + " ASC"
    };

    function initHead() {
        var thead = grid.querySelector("table thead");
        var tr = document.createElement("tr");
        settings.columns.forEach(function (c, i) {
            var sortable = c.sortable ? "sortable='" + c.field + "'" : "";
            var th = "<th " + sortable + ">" + c.text + "</th>";
            tr.innerHTML += th;
        });
        thead.appendChild(tr);
    }

    function initCriteria() {
        var select = grid.querySelector("[name='criteria']");
        settings.columns.forEach(function (c, i) {
            if (c.field) {
                var option = "<option value='" + c.field + "'>" + c.text + "</option>";
                select.innerHTML += option;
            }
        });
    }

    function loadData() {
        var body = JSON.stringify(option);
        $.ajax({
            url: settings.url,
            method: "POST",
            headers: { "Content-Type": "application/json" },
            data: body
        }).then(
            function (res) {
                if (res.meta.success) {
                    var tbody = grid.querySelector("table tbody");
                    tbody.innerHTML = "";
                    var rows = res.data.Rows;
                    rows.forEach(function (row, i) {
                        var tr = document.createElement("tr");
                        settings.columns.forEach(function (c, i) {
                            if (c.render) {
                                tr.innerHTML += "<td>" + c.render(row) + "</td>";
                            } else {
                                if (c.field) {
                                    tr.innerHTML += "<td>" + row[c.field] + "</td>";
                                } else {
                                    tr.innerHTML += "<td></td>";
                                }

                            }
                        });

                        tbody.appendChild(tr);
                        var actions = tr.querySelectorAll("td a");
                        actions.forEach(function (a, i) {
                            var action = a.attributes["action"].value;
                            if (settings[action]) {
                                a.onclick = function () {
                                    settings[action](row);
                                };
                            }
                        });
                    });

                    grid.querySelector("[row-count]").textContent = res.data.RowCount;
                    grid.querySelector("#page").textContent = option.page;
                    grid.querySelector("#btn-prev").disabled = option.page === 1;
                    grid.querySelector("#btn-next").disabled = option.page === res.data.PageCount;
                }
            },
            function (err) { console.log("Error: ", err); }
        );
    }

    initHead();
    initCriteria();
    loadData();

    grid.querySelector("#btn-prev").onclick = function () {
        option.page--;
        loadData();
    };

    grid.querySelector("#btn-next").onclick = function () {
        option.page++;
        loadData();
    };

    $("#page-size").change(function (e) {
        pgsz = e.target.value;
        option.page = 1;
        option.pageSize = pgsz;
        loadData();
    });

    var listth = grid.querySelectorAll("th[sortable]");
    listth.forEach(function (th, i) {
        var icon = document.createElement("i");
        icon.className = "th-icon glyphicon glyphicon-sort";
        th.appendChild(icon);
        th.onclick = function () {
            var col = th.attributes["sortable"].value;
            var ords = option.order.split(" ");
            if (ords[0] === col) {
                option.order = col + " " + (ords[1] === "ASC" ? "DESC" : "ASC");
            } else {
                option.order = col + " ASC";
            }
            loadData();
            //sett icon
            var icons = grid.querySelectorAll("th[sortable] i");
            icons.forEach(function (ic, i) {
                ic.className = "th-icon glyphicon glyphicon-sort";
                ic.style.opacity = "0.4";
                ic.parentElement.style.backgroundColor = "white";
            });
            ords = option.order.split(" ");
            var elic = grid.querySelector("[sortable='" + col + "'] i");
            elic.className = "th-icon glyphicon glyphicon-sort-by-attributes" + (ords[1] === "ASC" ? "" : "-alt");
            elic.style.opacity = "0.8";
            elic.parentElement.style.backgroundColor = "#eee";
        }
    });

    function showCriteria() {
        var crtList = grid.querySelector("[crt-list]");
        crtList.innerHTML = "";
        option.criteria.forEach(function (c, i) {
            crtList.innerHTML += "<div class='col-md-10'>Search <span class='crt-name'>'" + c.name + "'</span> Contains <span class='crt-value'>'" + c.value + "'</span> <a col-name='CustomerID'>Remove</a></div>";
        });

        var lista = crtList.querySelectorAll("a");
        lista.forEach(function (a, i) {
            a.onclick = function () {
                option.criteria.splice(i, 1);
                loadData();
                showCriteria();
            }
        });
    }

    function search() {
        var crt = {
            name: grid.querySelector("[name='criteria']").value,
            value: grid.querySelector("[name='value']").value
        };
        option.criteria.push(crt);
        loadData();
        showCriteria();
    }

    grid.querySelector("[btn-search]").onclick = search;

    return {
        reload: function () { loadData(); }
    };
}

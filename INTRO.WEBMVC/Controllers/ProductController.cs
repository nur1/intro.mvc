﻿using INTRO.DataAccess;
using INTRO.Entity;
using INTRO.Helper;
using INTRO.WEBMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace INTRO.WEBMVC.Controllers
{
    public class ProductController : Controller
    {
        // GET: Products
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int id)
        {
            using (var facade = new ProductFacade())
            {
                var product = facade.GetItembyID(id);
                return View(product);
            }
        }

        public ActionResult Edit(int id)
        {
            using (var facade = new ProductFacade())
            {
                var prod = facade.GetItembyID(id);
                var suppl = new SupplierFacade().GetItemByID(prod.SupplierID);
                var ctg = new CategoryFacade().GetItemByID(prod.CategoryID);
                var product = new ProductView {
                    ProductID = prod.ProductID,
                    ProductName = prod.ProductName,
                    SupplierID = prod.SupplierID,
                    CategoryID = prod.CategoryID,
                    QuantityPerUnit = prod.QuantityPerUnit,
                    UnitPrice = prod.UnitPrice,
                    UnitsInStock = prod.UnitsInStock,
                    UnitsOnOrder = prod.UnitsOnOrder,
                    ReorderLevel = prod.ReorderLevel,
                    Discontinued = prod.Discontinued,
                    SupplierName = suppl.CompanyName,
                    CategoryName = ctg.CategoryName
                };
                return View(product);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Update(ProductView product)
        {
            if (ModelState.IsValid)
            {
                using (var facade = new ProductFacade())
                {
                    var prd = new Products {
                        ProductID = product.ProductID,
                        ProductName = product.ProductName,
                        SupplierID = product.SupplierID,
                        CategoryID = product.CategoryID,
                        QuantityPerUnit = product.QuantityPerUnit,
                        UnitPrice = product.UnitPrice,
                        UnitsInStock = product.UnitsInStock,
                        UnitsOnOrder = product.UnitsOnOrder,
                        ReorderLevel = product.ReorderLevel,
                        Discontinued = product.Discontinued
                    };
                    bool status = await facade.Update(prd);
                    if (status)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View(product);
                    }
                }
            }
            else
            {
                return View(product);
            }

        }

        public ActionResult Add()
        {
            var product = new Products();
            return View(product);
        }

        [HttpPost]
        public async Task<ActionResult> Insert(ProductView product)
        {
            if (ModelState.IsValid)
            {
                using (var facade = new ProductFacade())
                {
                    var prod = new Products
                    {
                        ProductID = product.ProductID,
                        ProductName = product.ProductName,
                        SupplierID = product.SupplierID,
                        CategoryID = product.CategoryID,
                        QuantityPerUnit = product.QuantityPerUnit,
                        UnitPrice = product.UnitPrice,
                        UnitsInStock = product.UnitsInStock,
                        UnitsOnOrder = product.UnitsOnOrder,
                        ReorderLevel = product.ReorderLevel,
                        Discontinued = product.Discontinued
                    };
                    bool status = await facade.Insert(prod);
                    if (status)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View(product);
                    }
                }
            }
            else
            {
                return View(product);
            }

        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            if (ModelState.IsValid)
            {
                using (var facade = new ProductFacade())
                {
                    bool status = await facade.Delete(id);
                    if (status)
                    {
                        return Json(ApiResponse.OK("Id = " + id, "Item Deleted"));
                    }
                    else
                    {
                        return Json(ApiResponse.NotFound("Id = " + id, "Data not found"));
                    }
                }
            }
            else
            {
                return Json(ApiResponse.NotAcceptable("Id = " + id, "not Accepted"));
            }

        }

        [HttpPost]
        public async Task<ActionResult> Paging(PageOption option)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new ProductFacade())
                    {
                        var result = await facade.GetPage(option);
                        return Json(ApiResponse.OK(result, "OK"));
                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message, ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest("Bad Request", "Bad Request"));
            }
        }
    }
}
﻿using INTRO.DataAccess;
using INTRO.Entity;
using INTRO.Helper;
using INTRO.WEBMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace INTRO.WEBMVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        { 
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> GetPaging(PageOption option)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using(var facade = new CustomerFacade())
                    {
                        var result = await facade.GetPage(option);
                        return Json(result);
                    }
                }catch(Exception ex)
                {
                    return Json(ex.Message);
                }
            }
            else
            {
                return Json("Data not valid");
            }
        }

        [HttpGet]
        public ActionResult Hello()
        {
            return View();
        }

        public ActionResult GetHello(Contact contact)
        {
            return View(contact);
        }
    }
}
﻿using INTRO.DataAccess;
using INTRO.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace INTRO.WEBMVC.Controllers
{
    public class SupplierController : Controller
    {
        // GET: Supplier
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Paging(PageOption option)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new SupplierFacade())
                    {
                        var result = await facade.GetPage(option);
                        return Json(
                            new
                            {
                                meta = new { success = true, message = "OK", status = HttpStatusCode.OK, timestamp = DateTime.Now.Ticks },
                                data = result
                            });
                    }
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        meta = new { success = false, message = ex.Message, status = HttpStatusCode.InternalServerError, timestamp = DateTime.Now.Ticks },
                        data = ex.Message
                    });
                }
            }
            else
            {
                return Json(new
                {
                    meta = new { success = false, message = "Bad Request", status = HttpStatusCode.BadRequest, timestamp = DateTime.Now.Ticks },
                    data = "Bad Request"
                });
            }
        }
    }
}
﻿using INTRO.DataAccess;
using INTRO.Entity;
using INTRO.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace INTRO.WEBMVC.Controllers
{
    public class CategoryController : Controller
    { 
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int id)
        {
            using (var facade = new CategoryFacade())
            {
                var category = facade.GetItembyID(id);
                return View(category);
            }
        }

        public ActionResult Edit(int id)
        {
            using (var facade = new CategoryFacade())
            {
                var category = facade.GetItembyID(id);
                return View(category);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Update(CategoryView category)
        {
            if (ModelState.IsValid)
            {
                using (var facade = new CategoryFacade())
                {
                    bool status = await facade.Update(category);
                    if (status)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View(category);
                    }
                }
            }
            else
            {
                return View(category);
            }

        }

        public ActionResult Add()
        {
            var category = new CategoryView();
            return View(category);
        }

        [HttpPost]
        public async Task<ActionResult> Insert(CategoryView category)
        {
            if (ModelState.IsValid)
            {
                using (var facade = new CategoryFacade())
                {
                    bool status = await facade.Insert(category);
                    if (status)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View(category);
                    }
                }
            }
            else
            {
                return View(category);
            }

        }

        [HttpPost]
        public async Task<ActionResult> Delete(int id)
        {
            if (ModelState.IsValid)
            {
                using (var facade = new CategoryFacade())
                {
                    bool status = await facade.Delete(id);
                    if (status)
                    {
                        return Json(ApiResponse.OK("Id = " + id, "Item Deleted"));
                    }
                    else
                    {
                        return Json(ApiResponse.NotFound("Id = " + id, "Data not found"));
                    }
                }
            }
            else
            {
                return Json(ApiResponse.NotAcceptable("Id = " + id, "not Accepted"));
            }

        }

        [HttpPost]
        public async Task<ActionResult> Paging(PageOption option)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new CategoryFacade())
                    {
                        var result = await facade.GetPage(option);
                        return Json(ApiResponse.OK(result, "OK"));
                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message, ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest("Bad Request", "Bad Request"));
            }
        }
    }
}
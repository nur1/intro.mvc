﻿using INTRO.DataAccess;
using INTRO.Entity;
using INTRO.Helper;
using INTRO.WEBMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace INTRO.WEBMVC.Controllers
{
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(string id)
        {
            using (var facade = new CustomerFacade())
            {
                var customer = facade.GetItembyID(id);
                return View(customer);
            }
        }

        public ActionResult Edit(string id)
        {
            using (var facade = new CustomerFacade())
            {
                var customer = facade.GetItembyID(id);
                return View(customer);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Update(Customers customer)
        {
            if (ModelState.IsValid)
            {
                using (var facade = new CustomerFacade())
                {
                    bool status = await facade.Update(customer);
                    if (status)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View(customer);
                    }
                }
            }
            else
            {
                return View(customer);
            }

        }

        public ActionResult Add()
        {
            var customer = new Customers();
            return View(customer);
        }

        [HttpPost]
        public async Task<ActionResult> Insert(Customers customer)
        {
            if (ModelState.IsValid)
            {
                using (var facade = new CustomerFacade())
                {
                    bool status = await facade.Insert(customer);
                    if (status)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View(customer);
                    }
                }
            }
            else
            {
                return View(customer);
            }

        }

        [HttpPost]
        public async Task<ActionResult> Delete(string id)
        {
            if (ModelState.IsValid)
            {
                using (var facade = new CustomerFacade())
                {
                    bool status = await facade.Delete(id);
                    if (status)
                    {
                        return Json(ApiResponse.OK("Id = " + id, "Item Deleted"));
                    }
                    else
                    {
                        return Json(ApiResponse.NotFound("Id = " + id, "Data not found"));
                    }
                }
            }
            else
            {
                return Json(ApiResponse.NotAcceptable("Id = "+id, "not Accepted"));
            }

        }

        [HttpPost]
        public async Task<ActionResult> Paging(PageOption option)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    using (var facade = new CustomerFacade())
                    {
                        var result = await facade.GetPage(option);
                        return Json(ApiResponse.OK(result, "OK"));
                    }
                }
                catch (Exception ex)
                {
                    return Json(ApiResponse.InternalServerError(ex.Message, ex.Message));
                }
            }
            else
            {
                return Json(ApiResponse.BadRequest("Bad Request", "Bad Request"));
            }
        }
    }
}
﻿using INTRO.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTRO.DataAccess
{
    public class NorthwindContext : DbContext
    {

        public NorthwindContext() : base("NORTHWIND")
        {

        }

        public DbSet<Customers> Customers { get; set; }

        public DbSet<Suppliers> Suppliers { get; set; }

        public DbSet<Shippers> Shippers { get; set; }

        public DbSet<Products> Products { get; set; }
        public DbSet<Categories> Categories { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using INTRO.Entity;
using INTRO.Helper;
using System.Data.Entity;

namespace INTRO.DataAccess
{
    public class CustomerFacade : IDisposable
    {
        private NorthwindContext DB = null;

        public CustomerFacade()
        {
            DB = new NorthwindContext();
        }

        public List<Customers> GetAll()
        {
            return DB.Customers.ToList();
        }


        public async Task<PageResult<Customers>> GetPage(PageOption option)
        {
            try
            {
                var res = await Task.Run(() =>
                {
                    var where = " 1=1 ";
                    var param = new object[option.Criteria.Count];
                    int i = 0;
                    option.Criteria.ForEach(c => {
                        where += string.Format("AND {0}.ToString().Contains(@{1}) ", c.Name, i);
                        param[i] = c.Value;
                        i++;
                    });
                    var query = DB.Customers.Where(where, param);
                    var count = query.Count();
                    var rows = query.OrderBy(option.Order)
                        .Skip((option.Page - 1) * option.PageSize)
                        .Take(option.PageSize)
                        .ToList();
                    var pageCount = (int)Math.Ceiling(count * 1.0 / option.PageSize);
                    var result = new PageResult<Customers>(rows, count, pageCount);
                    return result;
                });
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<bool> Update(Customers customer)
        {
            try
            {
                DB.Entry<Customers>(customer).State = EntityState.Modified;
                int res = await DB.SaveChangesAsync();
                return res > 0;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Insert(Customers customer)
        {
            try
            {
                DB.Customers.Add(customer);
                int res = await DB.SaveChangesAsync();
                return res > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Customers GetItembyID(string id)
        {
            return DB.Customers.Find(id);
        }

        public void Dispose()
        {

        }

        public async Task<bool> Delete(string id)
        {
            try
            {
                var customer = GetItembyID(id);
                if(customer == null)
                {
                    throw new Exception("Data not found");
                }
                else
                {
                    DB.Customers.Remove(customer);
                    int res = await DB.SaveChangesAsync();
                    return res > 0;
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

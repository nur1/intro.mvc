﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using INTRO.Entity;
using INTRO.Helper;
using System.Data.Entity;

namespace INTRO.DataAccess
{
    public class CategoryFacade : IDisposable
    {
        private NorthwindContext DB = null;

        public CategoryFacade()
        {
            DB = new NorthwindContext();
        }

        public List<Categories> GetAll()
        {
            return DB.Categories.ToList();
        }


        public async Task<PageResult<CategoryView>> GetPage(PageOption option)
        {
            try
            {
                var res = await Task.Run(() =>
                {
                    var where = " 1=1 ";
                    var param = new object[option.Criteria.Count];
                    int i = 0;
                    option.Criteria.ForEach(c => {
                        where += string.Format("AND {0}.ToString().Contains(@{1}) ", c.Name, i);
                        param[i] = c.Value;
                        i++;
                    });
                    var query = DB.Categories.Where(where, param);
                    var count = query.Count();
                    var list = query.OrderBy(option.Order)
                        .Skip((option.Page - 1) * option.PageSize)
                        .Take(option.PageSize)
                        .ToList();
                    var rows = new List<CategoryView>();
                    list.ForEach(c => {
                        var str64 = Convert.ToBase64String(c.Picture, 78, c.Picture.Length - 78);
                        rows.Add(new CategoryView { CategoryID =c.CategoryID,
                        CategoryName = c.CategoryName,
                        Description = c.Description,
                        Picture = str64 });
                    });
                    var pageCount = (int)Math.Ceiling(count * 1.0 / option.PageSize);
                    var result = new PageResult<CategoryView>(rows, count, pageCount);
                    return result;
                });
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Categories GetItemByID(int id)
        {
            return DB.Categories.Find(id);
        }

        public async Task<bool> Update(CategoryView category)
        {
            try
            {
                var listPic = Convert.FromBase64String(category.Picture).ToList();
                for(int i=0; i < 78; i++)
                {
                    listPic.Insert(0, 0);
                }
                var ctg = new Categories { CategoryID = category.CategoryID,
                CategoryName = category.CategoryName,
                Description = category.Description,
                Picture = listPic.ToArray()
                };
                DB.Entry<Categories>(ctg).State = EntityState.Modified;
                int res = await DB.SaveChangesAsync();
                return res > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Insert(CategoryView category)
        {
            try
            {
                var listPic = Convert.FromBase64String(category.Picture).ToList();
                for (int i = 0; i < 78; i++)
                {
                    listPic.Insert(0, 0);
                }
                var ctg = new Categories
                {
                    CategoryID = category.CategoryID,
                    CategoryName = category.CategoryName,
                    Description = category.Description,
                    Picture = listPic.ToArray()
                };
                DB.Categories.Add(ctg);
                int res = await DB.SaveChangesAsync();
                return res > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Categories GetItembyID(int id)
        {
            return DB.Categories.Find(id);
        }

        public void Dispose()
        {

        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var category = GetItembyID(id);
                if (category == null)
                {
                    throw new Exception("Data not found");
                }
                else
                {
                    DB.Categories.Remove(category);
                    int res = await DB.SaveChangesAsync();
                    return res > 0;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

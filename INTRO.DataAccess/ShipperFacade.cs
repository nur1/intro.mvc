﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using INTRO.Entity;
using INTRO.Helper;

namespace INTRO.DataAccess
{
    public class ShipperFacade : IDisposable
    {
        private NorthwindContext DB = null;

        public ShipperFacade()
        {
            DB = new NorthwindContext();
        }

        public List<Shippers> GetAll()
        {
            return DB.Shippers.ToList();
        }


        public async Task<PageResult<Shippers>> GetPage(PageOption option)
        {
            try
            {
                var res = await Task.Run(() =>
                {
                    var where = " 1=1 ";
                    var param = new object[option.Criteria.Count];
                    int i = 0;
                    option.Criteria.ForEach(c =>
                    {
                        where += string.Format("AND {0}.ToString().Contains(@{1}) ", c.Name, i);
                        param[i] = c.Value;
                        i++;
                    });
                    var query = DB.Shippers.Where(where, param);
                    var count = query.Count();
                    var rows = query.OrderBy(option.Order)
                        .Skip((option.Page - 1) * option.PageSize)
                        .Take(option.PageSize)
                        .ToList();
                    var pageCount = (int)Math.Ceiling(count * 1.0 / option.PageSize);
                    var result = new PageResult<Shippers>(rows, count, pageCount);
                    return result;
                });
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Dispose()
        {

        }
    }
}

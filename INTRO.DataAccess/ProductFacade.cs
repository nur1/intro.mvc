﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using INTRO.Entity;
using INTRO.Helper;
using System.Data.Entity;

namespace INTRO.DataAccess
{
    public class ProductFacade : IDisposable
    {
        private NorthwindContext DB = null;

        public ProductFacade()
        {
            DB = new NorthwindContext();
        }

        public List<Products> GetAll()
        {
            return DB.Products.ToList();
        }


        public async Task<PageResult<ProductView>> GetPage(PageOption option)
        {
            try
            {
                var res = await Task.Run(() =>
                {
                    var where = " 1=1 ";
                    var param = new object[option.Criteria.Count];
                    int i = 0;
                    option.Criteria.ForEach(c => {
                        where += string.Format("AND {0}.ToString().Contains(@{1}) ", c.Name, i);
                        param[i] = c.Value;
                        i++;
                    });
                    var sql = @"SELECT * FROM (
	                                SELECT a.*, CompanyName [SupplierName], CategoryName 
	                                FROM Products a
	                                JOIN Suppliers b ON b.SupplierID=a.SupplierID
	                                JOIN Categories c ON c.CategoryID = a.CategoryID
                                )x";
                    var query = DB.Database.SqlQuery<ProductView>(sql).Where(where, param);
                    var count = query.Count();
                    var rows = query.OrderBy(option.Order)
                        .Skip((option.Page - 1) * option.PageSize)
                        .Take(option.PageSize)
                        .ToList();
                    var pageCount = (int)Math.Ceiling(count * 1.0 / option.PageSize);
                    var result = new PageResult<ProductView>(rows, count, pageCount);
                    return result;
                });
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<bool> Update(Products product)
        {
            try
            {
                DB.Entry<Products>(product).State = EntityState.Modified;
                int res = await DB.SaveChangesAsync();
                return res > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Insert(Products product)
        {
            try
            {
                DB.Products.Add(product);
                int res = await DB.SaveChangesAsync();
                return res > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Products GetItembyID(int id)
        {
            return DB.Products.Find(id);
        }

        public void Dispose()
        {

        }

        public async Task<bool> Delete(int id)
        {
            try
            {
                var product = GetItembyID(id);
                if (product == null)
                {
                    throw new Exception("Data not found");
                }
                else
                {
                    DB.Products.Remove(product);
                    int res = await DB.SaveChangesAsync();
                    return res > 0;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using INTRO.Entity;
using INTRO.Helper;

namespace INTRO.DataAccess
{
    public class SupplierFacade : IDisposable
    {
        private NorthwindContext DB = null;

        public SupplierFacade()
        {
            DB = new NorthwindContext();
        }

        public List<Suppliers> GetAll()
        {
            return DB.Suppliers.ToList();
        }

        public Suppliers GetItemByID(int id)
        {
            return DB.Suppliers.Find(id);
        }


        public async Task<PageResult<Suppliers>> GetPage(PageOption option)
        {
            try
            {
                var res = await Task.Run(() =>
                {
                    var where = " 1=1 ";
                    var param = new object[option.Criteria.Count];
                    int i = 0;
                    option.Criteria.ForEach(c => {
                        where += string.Format("AND {0}.ToString().Contains(@{1}) ", c.Name, i);
                        param[i] = c.Value;
                        i++;
                    });
                    var query = DB.Suppliers.Where(where, param);
                    var count = query.Count();
                    var rows = query.OrderBy(option.Order)
                        .Skip((option.Page - 1) * option.PageSize)
                        .Take(option.PageSize)
                        .ToList();
                    var pageCount = (int)Math.Ceiling(count * 1.0 / option.PageSize);
                    var result = new PageResult<Suppliers>(rows, count, pageCount);
                    return result;
                });
                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Dispose()
        {

        }
    }
}

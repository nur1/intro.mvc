﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace INTRO.Entity
{
    [Table("Categories")]
    public class Categories
    {
        [Key]
        public int CategoryID { get; set; }

        [Required]
        public string CategoryName { get; set; }

        [Required]
        public string Description { get; set; }
        public byte[] Picture { get; set; }
    }

    public class CategoryView
    { 
        public int CategoryID { get; set; }
         
        public string CategoryName { get; set; }
         
        public string Description { get; set; }
        public string Picture { get; set; }
    }
}

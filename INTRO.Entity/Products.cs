﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace INTRO.Entity
{
    [Table("Products")]
    public class Products
    {
        [Key]
        public int ProductID { get; set; }

        [Required]
        public string ProductName { get; set; }

        [Required]
        public int SupplierID { get; set; }

        [Required]
        public int CategoryID { get; set; }

        [Required]
        public string QuantityPerUnit { get; set; }

        [Required]
        public decimal UnitPrice { get; set; }

        [Required]
        public Int16 UnitsInStock { get; set; }

        public Int16 UnitsOnOrder { get; set; }

        public Int16 ReorderLevel { get; set; }

        public bool Discontinued { get; set; }
    }

    public class ProductView
    { 
        public int ProductID { get; set; }
         
        public string ProductName { get; set; }
         
        public int SupplierID { get; set; }

        public string SupplierName { get; set; }

        public int CategoryID { get; set; }

        public string CategoryName { get; set; }

        public string QuantityPerUnit { get; set; }
         
        public decimal UnitPrice { get; set; }
         
        public Int16 UnitsInStock { get; set; }

        public Int16 UnitsOnOrder { get; set; }

        public Int16 ReorderLevel { get; set; }

        public bool Discontinued { get; set; }
    }
}

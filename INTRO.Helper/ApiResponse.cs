﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace INTRO.Helper
{
    public class ApiResponse
    {
        public Meta meta { get; set; }

        public object data { get; set; }

        public ApiResponse()
        {
            meta = new Meta { timeSpan = DateTime.Now.Ticks };
        }

        public static ApiResponse OK(object Data)
        {
            return new ApiResponse {
                meta = new Meta { success = true, message = "", status = HttpStatusCode.OK, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse OK(object Data, string Message)
        {
            return new ApiResponse
            {
                meta = new Meta { success = true, message = Message, status = HttpStatusCode.OK, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse BadRequest(object Data)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = "", status = HttpStatusCode.BadRequest, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse BadRequest(object Data, string Message)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = Message, status = HttpStatusCode.BadRequest, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse InternalServerError(object Data)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = "", status = HttpStatusCode.InternalServerError, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse InternalServerError(object Data, string Message)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = Message, status = HttpStatusCode.InternalServerError, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse NotFound(object Data)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = "", status = HttpStatusCode.NotFound, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse NotFound(object Data, string Message)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = Message, status = HttpStatusCode.NotFound, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse Accepted(object Data)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = "", status = HttpStatusCode.Accepted, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse Accepted(object Data, string Message)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = Message, status = HttpStatusCode.Accepted, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse NotAcceptable(object Data)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = "", status = HttpStatusCode.NotAcceptable, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse NotAcceptable(object Data, string Message)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = Message, status = HttpStatusCode.NotAcceptable, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse Created(object Data)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = "", status = HttpStatusCode.Created, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse Created(object Data, string Message)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = Message, status = HttpStatusCode.Created, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse Forbidden(object Data)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = "", status = HttpStatusCode.Forbidden, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse Forbidden(object Data, string Message)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = Message, status = HttpStatusCode.Forbidden, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse NoContent(object Data)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = "", status = HttpStatusCode.NoContent, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }

        public static ApiResponse NoContent(object Data, string Message)
        {
            return new ApiResponse
            {
                meta = new Meta { success = false, message = Message, status = HttpStatusCode.NoContent, timeSpan = DateTime.Now.Ticks },
                data = Data
            };
        }
    }

    public class Meta
    {
        public bool success { get; set; }

        public HttpStatusCode status { get; set; }

        public string message { get; set; }

        public long timeSpan { get; set; }
    }
}

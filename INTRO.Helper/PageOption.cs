﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTRO.Helper
{
    public class PageOption
    {
        public int Page { get; set; }

        public int PageSize { get; set; }

        public List<Criteria> Criteria { get; set; }

        public string Order { get; set; }

        public PageOption() { Criteria = new List<Criteria>();  }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INTRO.Helper
{
    public class PageResult<TEntity>
    {
        public IList<TEntity> Rows { get; set; }

        public int RowCount { get; set; }

        public int PageCount { get; set; }

        public PageResult() { Rows = new List<TEntity>(); }

        public PageResult(IList<TEntity> rows, int rowCount, int pageCount)
        {
            Rows = rows;
            RowCount = rowCount;
            PageCount = pageCount;
        }
    }
}
